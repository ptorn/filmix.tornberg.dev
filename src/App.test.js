import React from 'react';
import { render } from '@testing-library/react';
import { Header } from './components/layout/Header';
import { SearchMovieContainer } from './containers/SearchMovie';

describe('Check if sub components has rendered', () => {
  test('renders Header title', () => {
    const { getByText } = render(<Header />);
    const title = getByText('Filmix - Peder Tornberg');
    expect(title).toBeInTheDocument();
  });

  test('renders welcome message', () => {
    const { getByText } = render(<SearchMovieContainer />);
    const welcome = getByText('Welcome to Filmix');
    expect(welcome).toBeInTheDocument();
  });
});
