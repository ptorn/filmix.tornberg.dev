import * as actions from '../../actions/watchLaterActions'
import * as types from '../../actions/types'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({
  watchLaterMovies: []
});
const movie = {
  adult: false,
  backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
  genreIds: [28, 878],
  id: 603,
  originalLanguage: 'en',
  originalTitle: 'The test movie',
  overview: 'Test description',
  popularity: 38.429,
  posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
  releaseDate: '1999-03-30',
  title: 'The test movie',
  video: false,
  voteAverage: 8.1,
  voteCount: 17216,
};

describe('watch later actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  test('should create an action to add a movie to watch later list', () => {
    const expectedActions = [{
      payload: movie,
      type: types.ADD_MOVIE_TO_WATCH_LATER,
    }];
    store.dispatch(actions.addMovieToWatchLater(movie));
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should create an action to remove a movie from watch later list', () => {
    const expectedActions = [{
      payload: movie,
      type: types.REMOVE_MOVIE_FROM_WATCH_LATER,
    }];
    store.dispatch(actions.removeMovieFromWatchLater(movie));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
