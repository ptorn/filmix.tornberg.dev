import * as actions from '../../actions/favoritActions'
import * as types from '../../actions/types'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({
  favoritMovies: []
});
const movie = {
  adult: false,
  backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
  genreIds: [28, 878],
  id: 603,
  originalLanguage: 'en',
  originalTitle: 'The test movie',
  overview: 'Test description',
  popularity: 38.429,
  posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
  releaseDate: '1999-03-30',
  title: 'The test movie',
  video: false,
  voteAverage: 8.1,
  voteCount: 17216,
};

describe('favorit actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  test('should create an action to add a movie to favorit list', () => {
    const expectedActions = [{
      payload: movie,
      type: types.ADD_MOVIE_TO_FAVORITS,
    }];
    store.dispatch(actions.addMovieToFavorits(movie));
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should create an action to remove a movie from favorit list', () => {
    const expectedActions = [{
      payload: movie,
      type: types.REMOVE_MOVIE_FROM_FAVORITS,
    }];
    store.dispatch(actions.removeMovieFromFavorits(movie));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
