import * as actions from '../../actions/searchActions'
import * as types from '../../actions/types'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axios from 'axios';
import { Movie, MovieSearchItem, MovieSearchResult } from '../../models/Movie';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const store = mockStore({
  searchValue: '',
  movies: {
    page: null,
    results: [],
    totalPages: null,
    totalResults: null
  },
});

jest.mock('axios', () => {
  return {
    get: jest.fn()
  };
});

describe('search actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  test('should creat action for search input', () => {
    const searchInput = 'Test movie';
    const expectedActions = [{
      type: types.SEARCH_INPUT_FOR_MOVIE,
      payload: searchInput
    }];
    store.dispatch(actions.setSearchInput(searchInput));
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should creat action for search value', () => {
    const searchValue = 'Test movie';
    const expectedActions = [{
      type: types.SEARCH_VALUE_FOR_MOVIE,
      payload: searchValue
    }];
    store.dispatch(actions.setSearchMovie(searchValue));
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should create an action to fetch a list of movies', async () => {
    (axios.get).mockResolvedValueOnce({
      data: {
        page: 1,
        results: [{
          adult: false,
          backdrop_path: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
          genre_ids: [28, 878],
          id: 603,
          original_language: 'en',
          original_title: 'The test movie',
          overview: 'Test description',
          popularity: 38.429,
          poster_path: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
          release_date: '1999-03-30',
          title: 'The test movie',
          video: false,
          vote_average: 8.1,
          vote_count: 17216,
        }],
        total_pages: 1,
        total_results: 0
      }
    });
    const expectedActions = [{
      payload: new MovieSearchResult({
        page: 1,
        results: [new MovieSearchItem({
          adult: false,
          backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
          genreIds: [28, 878],
          id: 603,
          originalLanguage: 'en',
          originalTitle: 'The test movie',
          overview: 'Test description',
          popularity: 38.429,
          posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
          releaseDate: '1999-03-30',
          title: 'The test movie',
          video: false,
          voteAverage: 8.1,
          voteCount: 17216,
        })],
        totalPages: 1,
        totalResults: 0
      }),
      type: types.GET_MOVIES,
    },
    {
      type: types.SEARCH_VALUE_FOR_MOVIE,
      payload: 'test movie'
    },
    {
      type: types.SEARCH_INPUT_FOR_MOVIE,
      payload: 'test movie'
    }];
    await store.dispatch(actions.getMovies('test movie')).then(() => {});
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should create an action for failed request', async () => {
    (axios.get).mockRejectedValueOnce(new Error('Fetch error'));
    const expectedActions = [{
      payload: 'An error has occured',
      type: types.GET_MOVIES_FAILURE
    }];
    await store.dispatch(actions.getMovies('test movie')).then(() => {});
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should creat action for reset search movie', () => {
    const expectedActions = [{
      type: types.RESET_SEARCH_MOVIE,
      payload: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      }
    },
    {
      type: types.SEARCH_VALUE_FOR_MOVIE,
      payload: ''
    },
    {
      type: types.SEARCH_INPUT_FOR_MOVIE,
      payload: ''
    }];
    store.dispatch(actions.resetSearchMovie());
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should create an action to fetch a movie', async () => {
    (axios.get).mockResolvedValueOnce({
      data: {
        adult: false,
        backdrop_path: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
        belongs_to_collection: {
          backdrop_path: '/bRm2DEgUiYciDw3myHuYFInD7la.jpg',
          id: 1234,
          name: 'The test Collection',
          poster_path: '/lh4aGpd3U9rm9B8Oqr6CUgQLtZL.jpg'
        },
        budget: 1234,
        genres: [{
          id: 28,
          name: "Action"
        }],
        homepage: 'http://www.example.com',
        id: 603,
        imdb_id: '12345678',
        original_language: 'en',
        original_title: 'The test movie',
        overview: 'Test description',
        popularity: 38.429,
        poster_path: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
        production_companies: [],
        production_countries: [],
        release_date: '1999-03-30',
        revenue: 1234567,
        runtime: 116,
        spoken_languages: [],
        status: 'Released',
        tagline: 'Awesome',
        title: 'The test movie',
        video: false,
        videos: { results: [] },
        vote_average: 8.1,
        vote_count: 17216,
      }
    });
    const expectedActions = [{
      payload: new Movie({
        adult: false,
        backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
        belongsToCollection: {
          backdropPath: '/bRm2DEgUiYciDw3myHuYFInD7la.jpg',
          id: 1234,
          name: 'The test Collection',
          posterPath: '/lh4aGpd3U9rm9B8Oqr6CUgQLtZL.jpg'
        },
        budget: 1234,
        genres: [{
          id: 28,
          name: "Action"
        }],
        homepage: 'http://www.example.com',
        id: 603,
        imdbId: '12345678',
        originalLanguage: 'en',
        originalTitle: 'The test movie',
        overview: 'Test description',
        popularity: 38.429,
        posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
        productionCompanies: [],
        productionCountries: [],
        releaseDate: '1999-03-30',
        revenue: 1234567,
        runtime: 116,
        spokenLanguages: [],
        status: 'Released',
        tagline: 'Awesome',
        title: 'The test movie',
        video: false,
        videos: [],
        voteAverage: 8.1,
        voteCount: 17216,
      }),
      type: types.GET_MOVIE
    }];
    await store.dispatch(actions.getMovie(603)).then(() => {});
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('should creat action for reset the movie', () => {
    const expectedActions = [{
      type: types.RESET_MOVIE,
      payload: null
    }];
    store.dispatch(actions.resetMovie());
    expect(store.getActions()).toEqual(expectedActions);
  });
})
