import React from 'react';
import { render } from '@testing-library/react';
import MovieCard from '../MovieCard';
import { MovieGenre, MovieBelongsToCollection, Movie } from '../../../models/Movie';

describe('rendering movie card', () => {
  const movie = new Movie({
    adult: false,
    backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
    belongsToCollection: new MovieBelongsToCollection(),
    budget: 1234,
    genres: [new MovieGenre({
      id: 28,
      name: "Action"
    })],
    homepage: 'http://www.example.com',
    id: 603,
    imdbId: '12345678',
    originalLanguage: 'en',
    originalTitle: 'The test movie',
    overview: 'Test description',
    popularity: 38.429,
    posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
    productionCompanies: [],
    productionCountries: [],
    releaseDate: '1999-03-30',
    revenue: 1234567,
    runtime: 116,
    spokenLanguages: [],
    status: 'Released',
    tagline: 'Awesome',
    title: 'The test movie',
    video: false,
    videos: [],
    voteAverage: 8.1,
    voteCount: 17216,
  });
  const addMovieToFavorits = () => {};
  const addMovieToWatchLater = () => {};
  const removeMovieFromFavorits = () => {};
  const removeMovieFromWatchLater = () => {};

  test('renders a card with a movie and a description', () => {
    const { getByText } = render(<MovieCard movie={ movie } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      favoritMovies={ [] } watchLaterMovies={ [] } />);
    const movieDescription = getByText(/Test description/i);
    expect(movieDescription).toBeInTheDocument();
  });

  test('renders a movie card with icon add to favorits', () => {
    const { getByText } = render(<MovieCard movie={ movie } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      favoritMovies={ [] } watchLaterMovies={ [] } />);
    const favoritsLogo = getByText(/star/i);
    expect(favoritsLogo).toBeInTheDocument();
  });

  test('renders movie item with icon add to watch later', () => {
    const { getByText } = render(<MovieCard movie={ movie } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      favoritMovies={ [] } watchLaterMovies={ [] } />);
    const watchLaterLogo = getByText(/watch_later/i);
    expect(watchLaterLogo).toBeInTheDocument();
  });
});
