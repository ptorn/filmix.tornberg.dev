import React from 'react';
import { render } from '@testing-library/react';
import { SearchForm } from '../SearchForm';

const onSubmit = () => {};
const onChange = () => {};

describe('rendering search form', () => {
  test('renders search input label', () => {
    const { getByLabelText } = render(<SearchForm onSubmit={ onSubmit } onChange={ onChange } />);
    const inputLabel = getByLabelText(/search-input/i);
    expect(inputLabel).toBeInTheDocument();
  });

  test('renders search button', () => {
    const { getByText } = render(<SearchForm onSubmit={ onSubmit } onChange={ onChange } />);
    const searchButton = getByText('search');
    expect(searchButton).toBeInTheDocument();
  });
});
