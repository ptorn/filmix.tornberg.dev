import React from 'react';
import { render } from '@testing-library/react';
import { Header } from '../Header';

describe('Header toolbar', () => {
  test('renders Header title', () => {
    const { getByText } = render(<Header />);
    const title = getByText('Filmix - Peder Tornberg');
    expect(title).toBeInTheDocument();
  });
});
