import React from 'react';
import { render } from '@testing-library/react';
import { ErrorAlert } from '../errorAlert';

describe('ErrorAlert', () => {
  test('renders an ErrorAlert', () => {
    const { getByText } = render(<ErrorAlert error={ 'An error has occured' } />);
    const title = getByText('An error has occured');
    expect(title).toBeInTheDocument();
  });
});