import React from 'react';
import { render } from '@testing-library/react';
import { MovieList } from '../MovieList';
import { BrowserRouter as Router } from 'react-router-dom';
import { MovieSearchItem } from '../../../models/Movie';

describe('rendering movie list', () => {
  const movies = [new MovieSearchItem({
    adult: false,
    backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
    genreIds: [28, 878],
    id: 1,
    originalLanguage: 'en',
    originalTitle: 'The test movie',
    overview: 'Test description',
    popularity: 38.429,
    posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
    releaseDate: '1999-03-30',
    title: 'Test title',
    video: false,
    voteAverage: 8.1,
    voteCount: 17216,
  })];

  test('renders MovieList with a movie', () => {
    const { getByText } = render(<Router><MovieList movies={ movies } /></Router>);
    const movieTitle = getByText(/Test title/i);
    expect(movieTitle).toBeInTheDocument();
  });

  test('renders a delete button', () => {
    const { getByText } = render(<Router><MovieList movies={ movies } removeMovie={ () => {} } /></Router>);
    const deleteButton = getByText(/delete/i);
    expect(deleteButton).toBeInTheDocument();
  });

  test('renders without a delete button', () => {
    const { queryByText } = render(<Router><MovieList movies={ movies } /></Router>);
    const deleteButton = queryByText(/delete/i);
    expect(deleteButton).toBeNull();
  });

  test('does not render a list item title if no movies are in the list', () => {
    const movies = [];
    const { queryByText } = render(<Router><MovieList movies={ movies } removeMovie={ () => {} } /></Router>);
    const movieTitle = queryByText(/Test title/i);
    expect(movieTitle).toBeNull();
  });

  test('does not render a list item delete if no movies are in the list', () => {
    const movies = [];
    const { queryByText } = render(<Router><MovieList movies={ movies } /></Router>);
    const deleteButton = queryByText(/delete/i);
    expect(deleteButton).toBeNull();
  });
});
