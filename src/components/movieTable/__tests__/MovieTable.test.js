import React from 'react';
import { render } from '@testing-library/react';
import { MovieTable } from '../MovieTable';
import { MovieSearchItem } from '../../../models/Movie';

const movies = [new MovieSearchItem({
  adult: false,
  backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
  genreIds: [28, 878],
  id: 603,
  originalLanguage: 'en',
  originalTitle: 'The test movie',
  overview: 'Test description',
  popularity: 38.429,
  posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
  releaseDate: '1999-03-30',
  title: 'The test movie',
  video: false,
  voteAverage: 8.1,
  voteCount: 17216,
})];

const addMovieToFavorits = () => {};
const addMovieToWatchLater = () => {};
const removeMovieFromFavorits = () => {};
const removeMovieFromWatchLater = () => {};
const navigateToPage = () => {};

describe('rendering movie table', () => {
  test('renders movie table', () => {
    const { getByLabelText } = render(<MovieTable movies={ movies } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      navigateToPage={ navigateToPage } favoritMovies={ [] } watchLaterMovies={ [] } />);
    const lastPage = getByLabelText(/Movie table/i);
    expect(lastPage).toBeInTheDocument();
  });

  test('renders MovieTable with a movie', () => {
    const { getByText } = render(<MovieTable movies={ movies } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      navigateToPage={ navigateToPage } favoritMovies={ [] } watchLaterMovies={ [] } />);
    const movieTitle = getByText(/The test movie/i);
    expect(movieTitle).toBeInTheDocument();
  });

  test('renders movie item with icon add to favorits', () => {
    const { getByText } = render(<MovieTable movies={ movies } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      navigateToPage={ navigateToPage } favoritMovies={ [] } watchLaterMovies={ [] } />);
    const favoritsLogo = getByText(/star/i);
    expect(favoritsLogo).toBeInTheDocument();
  });

  test('renders movie item with icon add to watch later', () => {
    const { getByText } = render(<MovieTable movies={ movies } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      navigateToPage={ navigateToPage } favoritMovies={ [] } watchLaterMovies={ [] } />);
    const watchLaterLogo = getByText(/watch_later/i);
    expect(watchLaterLogo).toBeInTheDocument();
  });

  test('renders movie item with poster image', () => {
    const { container } = render(<MovieTable movies={ movies } totalCount={ 1 } page={ 0 } addMovieToFavorits={ addMovieToFavorits }
      addMovieToWatchLater={ addMovieToWatchLater } removeMovieFromFavorits={ removeMovieFromFavorits } removeMovieFromWatchLater={ removeMovieFromWatchLater }
      navigateToPage={ navigateToPage } favoritMovies={ [] } watchLaterMovies={ [] } />);
    expect(container.getElementsByTagName('img')).toHaveLength(1);
  });
});
