import React from 'react';
import { render } from '@testing-library/react';
import { MovieTablePaginatiorActions } from '../MovieTablePaginatorActions';

const onChangePage = () => {};

describe('rendering movie table paginator actions', () => {
  test('renders next page icon', () => {
    const { getByLabelText } = render(<MovieTablePaginatiorActions rowsPerPage={ 20 } count={ 100 } page={ 1 } onChangePage={ onChangePage } />);
    const nextPage = getByLabelText(/next page/i);
    expect(nextPage).toBeInTheDocument();
  });

  test('renders previous page icon', () => {
    const { getByLabelText } = render(<MovieTablePaginatiorActions rowsPerPage={ 20 } count={ 100 } page={ 1 } onChangePage={ onChangePage } />);
    const previousPage = getByLabelText(/previous page/i);
    expect(previousPage).toBeInTheDocument();
  });

  test('renders first page icon', () => {
    const { getByLabelText } = render(<MovieTablePaginatiorActions rowsPerPage={ 20 } count={ 100 } page={ 1 } onChangePage={ onChangePage } />);
    const firstPage = getByLabelText(/first page/i);
    expect(firstPage).toBeInTheDocument();
  });

  test('renders last page icon', () => {
    const { getByLabelText } = render(<MovieTablePaginatiorActions rowsPerPage={ 20 } count={ 100 } page={ 1 } onChangePage={ onChangePage } />);
    const lastPage = getByLabelText(/last page/i);
    expect(lastPage).toBeInTheDocument();
  });
});
