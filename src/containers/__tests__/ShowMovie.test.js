import React from 'react';
import { render } from '@testing-library/react';
import ShowMovieContainer from '../SearchMovie';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history'
import { Provider } from 'react-redux';
import store from '../../store';

describe('rendering show container', () => {
  test('renders page title', () => {
    const history = createMemoryHistory()
    const { getByText } = render(<Provider store={ store }><Router history={ history }><ShowMovieContainer /></Router></Provider>);
    const pageTitle = getByText('Welcome to Filmix');
    expect(pageTitle).toBeInTheDocument();
  });
});
