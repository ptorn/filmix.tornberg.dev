import React from 'react';
import { render } from '@testing-library/react';
import { SearchMovieContainer } from '../SearchMovie';

describe('rendering search container', () => {
  test('renders welcome message', () => {
    const { getByText } = render(<SearchMovieContainer />);
    const welcome = getByText('Welcome to Filmix');
    expect(welcome).toBeInTheDocument();
  });
});
