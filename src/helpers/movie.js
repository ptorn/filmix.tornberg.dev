import { MovieSearchItem } from "../models/Movie";

export const convertMovieShowToIndexObject = showMovieItem => {
  const movieSearchItem = new MovieSearchItem(showMovieItem);
  movieSearchItem.genreIds = showMovieItem.genres.map(genre => genre.id);
  return movieSearchItem;
}
