import reducer from '../watchLaterReducer'
import * as types from '../../actions/types'

const movie = {
  adult: false,
  backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
  genreIds: [28, 878],
  id: 603,
  originalLanguage: 'en',
  originalTitle: 'The test movie',
  overview: 'Test description',
  popularity: 38.429,
  posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
  releaseDate: '1999-03-30',
  title: 'The test movie',
  video: false,
  voteAverage: 8.1,
  voteCount: 17216,
};

describe('watch later reducer', () => {
  test('the initial state returned', () => {
    expect(reducer(undefined, {})).toEqual({
      watchLaterMovies: []
    });
  });

  test('should handle to add a watch later movie', () => {
    expect(reducer(undefined, {
      type: types.ADD_MOVIE_TO_WATCH_LATER,
      payload: movie
    })).toEqual({
      watchLaterMovies: [movie]
    });
  });

  test('should handle to remove a watch later movie', () => {
    expect(reducer(undefined, {
      type: types.REMOVE_MOVIE_FROM_WATCH_LATER,
      payload: movie
    })).toEqual({
      watchLaterMovies: []
    });
  });
});
