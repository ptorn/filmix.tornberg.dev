import reducer from '../searchReducer'
import * as types from '../../actions/types'
import { Movie } from '../../models/Movie';

describe('search reducer', () => {
  test('the initial state returned', () => {
    expect(reducer(undefined, {})).toEqual({
      searchInput: '',
      searchValue: '',
      movie: null,
      movies: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      },
      error: null
    });
  });

  test('should handle setting the search input', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_INPUT_FOR_MOVIE,
      payload: 'Test title'
    })).toEqual({
      searchInput: 'Test title',
      searchValue: '',
      movie: null,
      movies: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      },
      error: null
    });
  });

  test('should handle setting the search value', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_VALUE_FOR_MOVIE,
      payload: 'Test title'
    })).toEqual({
      searchInput: '',
      searchValue: 'Test title',
      movie: null,
      movies: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      },
      error: null
    });
  });

  test('should handle get movies', () => {
    const payload = {
      page: 1,
      results: [{
        adult: false,
        backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
        genreIds: [28, 878],
        id: 603,
        originalLanguage: 'en',
        originalTitle: 'The test movie',
        overview: 'Test description',
        popularity: 38.429,
        posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
        releaseDate: '1999-03-30',
        title: 'The test movie',
        video: false,
        voteAverage: 8.1,
        voteCount: 17216,
      }],
      totalPages: 1,
      totalResults: 0
    }
    expect(reducer(undefined, {
      type: types.GET_MOVIES,
      payload
    })).toEqual({
      searchInput: '',
      searchValue: '',
      movie: null,
      movies: payload,
      error: null
    });
  });

  test('should handle reset search movie', () => {
    const payload = {
      page: null,
      results: [],
      totalPages: null,
      totalResults: null
    }
    expect(reducer(undefined, {
      type: types.RESET_SEARCH_MOVIE,
      payload
    })).toEqual({
      searchInput: '',
      searchValue: '',
      movie: null,
      movies: payload,
      error: null
    });
  });

  test('should handle to get a movie', () => {
    const payload = new Movie({
      adult: false,
      backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
      belongsToCollection: {},
      budget: 1234,
      genres: [28, 878],
      homepage: 'http://www.example.com',
      id: 603,
      imdbId: '12345678',
      originalLanguage: 'en',
      originalTitle: 'The test movie',
      overview: 'Test description',
      popularity: 38.429,
      posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
      productionCompanies: [],
      productionCountries: [],
      releaseDate: '1999-03-30',
      revenue: 1234567,
      runtime: 116,
      spokenLanguages: [],
      status: 'Released',
      tagline: 'Awesome',
      title: 'The test movie',
      video: false,
      videos: [],
      voteAverage: 8.1,
      voteCount: 17216,
    });
    expect(reducer(undefined, {
      type: types.GET_MOVIE,
      payload
    })).toEqual({
      searchInput: '',
      searchValue: '',
      movie: payload,
      movies: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      },
      error: null
    });
  });

  test('should handle reset show movie', () => {
    expect(reducer(undefined, {
      type: types.RESET_MOVIE,
      payload: null
    })).toEqual({
      searchInput: '',
      searchValue: '',
      movie: null,
      movies: {
        page: null,
        results: [],
        totalPages: null,
        totalResults: null
      },
      error: null
    });
  });

});
