import reducer from '../favoritReducer'
import * as types from '../../actions/types'

const movie = {
  adult: false,
  backdropPath: '/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg',
  genreIds: [28, 878],
  id: 603,
  originalLanguage: 'en',
  originalTitle: 'The test movie',
  overview: 'Test description',
  popularity: 38.429,
  posterPath: '/f89U3ADr1oiB1s9GkdPOEpXUk5H.jpg',
  releaseDate: '1999-03-30',
  title: 'The test movie',
  video: false,
  voteAverage: 8.1,
  voteCount: 17216,
};

describe('favorits reducer', () => {
  test('the initial state returned', () => {
    expect(reducer(undefined, {})).toEqual({
      favoritMovies: []
    });
  });

  test('should handle to add a favorit movie', () => {
    expect(reducer(undefined, {
      type: types.ADD_MOVIE_TO_FAVORITS,
      payload: movie
    })).toEqual({
      favoritMovies: [movie]
    });
  });

  test('should handle to remove a favorit movie', () => {
    expect(reducer(undefined, {
      type: types.REMOVE_MOVIE_FROM_FAVORITS,
      payload: movie
    })).toEqual({
      favoritMovies: []
    });
  });
});
